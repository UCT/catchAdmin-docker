FROM ubuntu:focal

LABEL Maintainer="patrick <contact@uctoo.com>" \
      Description="UCToo container with Nginx & PHP-FPM based on Ubuntu focal."

ARG DEBIAN_FRONTEND=noninteractive
ARG TZ=Asia/Shanghai

RUN apt-get update --no-install-recommends &&  \
    apt-get install -y --no-install-recommends tzdata apt-utils && ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone && \
    apt-get install -y --no-install-recommends ca-certificates nano php-fpm nginx cron openssl php-mysql php-gd php-bcmath php-mbstring php-xml php-curl php-exif curl wget unzip git gettext && \
    rm -rf /var/lib/apt/lists/* && \
    sed -i '/session    required     pam_loginuid.so/c\#session    required   pam_loginuid.so' /etc/pam.d/cron

COPY root /
RUN chown -R www-data:www-data /var/www && rm -rf /var/www/html/*
WORKDIR /var/www/html
RUN git clone https://gitee.com/UCT/catchAdmin.git
COPY root/var/www/html/.example.env /var/www/html/catchAdmin/.env
RUN chmod 777 -R /var/www && chmod 777 -R /usr/sbin && cd /var/www/html/catchAdmin && wget -O composer-setup.php https://getcomposer.org/installer && php composer-setup.php --install-dir=/usr/local/bin --filename=composer && composer install --ignore-platform-reqs -n

EXPOSE 80 443
CMD ["/usr/sbin/supervisord", "-c", "/etc/supervisord.conf"]